import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Components
import { HomePageComponent } from './home-page.component';
import { HeaderComponent } from './header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FooterComponent } from './footer/footer.component';
import { HomeContentComponent } from './home-content/home-content.component';
import { HomeBannerComponent } from './home-banner/home-banner.component';

// Routes
const routes: Routes = [ 
  {
    path: '', component: HomePageComponent,
    children: [
      { path: '', component: HomeContentComponent },
      
      // { path: '', redirectTo: 'users', pathMatch: 'full'},
      // { path: 'movies', component: MoviesComponent},
    ],
  }
]

@NgModule({
  declarations: [
    HomePageComponent,
    HeaderComponent,
    PageNotFoundComponent,
    FooterComponent,
    HomeContentComponent,
    HomeBannerComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
})
export class HomePageModule { }
