import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Components
import { AdminComponent } from './admin.component';

// Routes
const routes: Routes = [ 
  {
    path: '', component: AdminComponent,
    children: [
      // { path: '', redirectTo: 'users', pathMatch: 'full'},
      // { path: 'users', component: UsersComponent},
      // { path: 'movies', component: MoviesComponent},
    ],
  }
]

@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class AdminModule { }
