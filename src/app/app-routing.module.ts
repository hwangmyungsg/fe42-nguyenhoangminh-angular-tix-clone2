import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Module
import { HomePageModule } from './home-page/home-page.module';
import { AdminModule } from './admin/admin.module';
// Component
import { PageNotFoundComponent } from './home-page/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', loadChildren: () => HomePageModule },
  { path: 'admin', loadChildren: () => AdminModule },

  // 404
  { path: '**', component: PageNotFoundComponent}

  // Lazy load
  // { path: 'admin', loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule)},
  // { path: 'home', component: () => HomePageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }